import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DocumentcollectorService {
	
	allDocUrl:string = "http://localhost:8190/api/documents/"

	constructor(private httpClient:HttpClient) {
	  
	}
  
  	getDocuments(callback) {
		// Collect documents from api
		this.httpClient.get(this.allDocUrl).subscribe(
			(data:any) => {
				//console.log(data)
				if (data != "") {
					console.log("Service fetched documents successfully");
					// send documents back through callback
					callback(data.Documents);
				} else {
					console.log("Service failed document fetch");
					callback(null);
				}
			}
		)
	}
	
	getDocumentPages(doc, callback) {
		var docID = doc.ID
		console.log("Collecting pages of doc with id: " + docID);
		
		var url = this.allDocUrl + docID + "/pages";
		
		// Collect pages from api
		this.httpClient.get(url).subscribe(
			(data:any) => {
				if (data != "") {
					console.log("Service fetched pages successfully");
					callback(data)
				} else {
					console.log("Service failed pages fetch");
					callback(null);
				}
			}
		)
	}
}
