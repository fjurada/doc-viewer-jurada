import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 

import { AppComponent } from './app.component';
import { DocumentlistComponent } from './documentlist/documentlist.component';
import { PageslistComponent } from './pageslist/pageslist.component';
import { PageviewComponent } from './pageview/pageview.component';
import { AdddocumentComponent } from './adddocument/adddocument.component';
import { AddpageComponent } from './addpage/addpage.component';

@NgModule({
  declarations: [
    AppComponent,
    DocumentlistComponent,
    PageslistComponent,
    PageviewComponent,
    AdddocumentComponent,
    AddpageComponent
  ],
  imports: [
    BrowserModule,
	HttpClientModule,
	FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
