export class DocTypes {
	DOCTYPE:any[];
	
	constructor() {
		this.DOCTYPE = [
			{"key": "0", "name": "NoType"},
			{"key": "10", "name": "Form"},
			{"key": "20", "name": "Invoice"},
			{"key": "30", "name": "IDCard"}
		];
	}
	
	getTypename(typecode) {
		var returnType = "NoType"
		typecode = typecode.toString();
		this.DOCTYPE.forEach(function(type) {
			if (typecode == type.key) {
				returnType =  type.name;
				return;
			}
		})
		return returnType;
	}
}