import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DocTypes } from '../doctypes';

@Component({
  selector: 'app-documentlist',
  templateUrl: './documentlist.component.html',
  styleUrls: ['./documentlist.component.css']
})
export class DocumentlistComponent implements OnInit {
	
	// For access within ngFor loops
	docTypes:any = new DocTypes();

	constructor() { }

	@Input() documents:any[];
	@Input() paginatedDocs:any[];
	@Input() paginatedDoctables:any[];

	@Output() selectEvent = new EventEmitter<any>();

	docSelected:any;
	paginateSelected:Number = 0;

	selectDocument(doc) {
		//this.selectFirst = false;
		console.log("Document "+ doc.Name +" selected"),
		this.docSelected = doc;
		this.sendSelectedDoc();
	}
	
	openTab($evt) {
		var elem = $evt.srcElement;
		var tabNum = elem.innerHTML;
		this.paginateSelected = Number(tabNum) - 1;
	}

	sendSelectedDoc() {
		this.selectEvent.emit(this.docSelected);
	}
	
	getTypename(typecode) {
		return this.docTypes.getTypename(typecode);
	}


	ngOnInit() {
	  
	}

}
