import { TestBed } from '@angular/core/testing';

import { DocumentcollectorService } from './documentcollector.service';

describe('DocumentcollectorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DocumentcollectorService = TestBed.get(DocumentcollectorService);
    expect(service).toBeTruthy();
  });
});
