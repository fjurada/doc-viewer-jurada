import { Component, OnInit, ViewChild, AfterViewInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DocumentcollectorService } from './documentcollector.service'

const BLANKPAGE = {
	"ID": 0,
	"ImageUrl": "",
	"DocumentID": 0,
	"Text": ""
}

const BLANKDOCUMENT = { 
	"ID": 0,
	"DocumentType": 0,
	"Name": "",
	"Pages": [BLANKPAGE]
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
	
	@ViewChild("no_content") noContentTemplate;
	
	// Initialize blank documents and pagess info before fetching data
	documents:any[] = [BLANKDOCUMENT];
	paginatedDocuments:any[] = [[BLANKDOCUMENT]];
	selectedDocument = BLANKDOCUMENT;
	pages:any[] = [BLANKPAGE];
	singlePage:any = BLANKPAGE;
	
	name:string = "Empty";
	allDocUrl:string = "http://localhost:8190/api/documents/"

	// Control reception of data
	docCollectSuccess:boolean = false;
	pagesCollectSuccess:boolean = false;
	
	// Count of pages to paginate
	paginatedDoctables:any[] = [];
	docsPerPage = 3;
	
	constructor (private documentCollectorService: DocumentcollectorService) {  }
	
	/////////////////////// Child component communication methods /////////////////////////////
	receiveDocumentSelection($event) {
		//console.log("Parent component received document: "+$event.Name);
		this.selectedDocument = $event;
		this.getPagesOfDoc(this.selectedDocument);
	}
	
	receivePageSelection($event) {
		//console.log("Parent component received page: "+$event.Text);
		this.singlePage = $event;
		this.showPage(this.singlePage.ID);
	}
	
	
	///////////// Document Collection service calling methods and callbacks ////////////
	
	/////// Documents ///////
	getDocs(): void {
		this.documentCollectorService.getDocuments(this.prepareDocuments.bind(this));
	}
	
	// Callback to receive documents
	prepareDocuments(documents) {
		
		if (documents == null) {
			console.log("No documents received");
			return;
		}
		
		this.documents = documents;
		this.paginateDocuments();
		
		// List first document received
		this.getPagesOfDoc(this.documents[0]);
		this.docCollectSuccess = true;
	}
	
	//////// Pages /////////
	getPagesOfDoc(document): void {
		this.documentCollectorService.getDocumentPages(document, this.preparePages.bind(this));
	}
	
	// Callback to receive pages
	preparePages(pages) {
		if (pages == null) {
			console.log("No Pages received");
			return;
		}
		
		this.pages = pages;
		this.pagesCollectSuccess = true;
		this.showPage(this.pages[0].ID)
	}
	
	////////////////////////////////////////////////////////////////////////////////////
	
	
	/////// Single page showing ////////
	showPage(pageID) {
		var pageToShow = null;
		this.pages.forEach(function (pg) {
			if (pg.ID == pageID) {
				pageToShow = pg;
			}
		});
		
		this.singlePage = pageToShow;
	}
	
	
	///////// Utility methods /////////////
	resetPagesView() {
		this.pages = [BLANKPAGE];
	}
	
	resetSinglePageView() {
		this.singlePage = BLANKPAGE;
	}
	
	
	///////// Custom pagination related methods //////////
	paginateDocuments() {
		var i = 0;
		var pages = [];
		var intPages=0;
		this.paginatedDocuments = [];
		var docPaginatedCollection = [];
		console.log("Paginating documents... number of docs: "+this.documents.length);
		while (i < this.documents.length) {
			docPaginatedCollection.push(this.documents[i]);
			if ( i>0 && ((i+1)%this.docsPerPage)==0 ) {
				this.paginatedDocuments.push(docPaginatedCollection);
				docPaginatedCollection = [];
				
				intPages = intPages+1;
				pages.push(intPages);
			}
			i = i+1;
		}
		
		// Set remaining documents into field
		if (docPaginatedCollection.length > 0) {
			this.paginatedDocuments.push(docPaginatedCollection);
			pages.push(intPages+1);
		}
		
		this.paginatedDoctables = pages;
	}
	
	////////// Interface methods ///////////
	ngOnInit() {
		
		// Trigger service to collect document data (retrieved through callback)
		this.getDocs();
	}
	
	ngAfterViewInit() {
		console.log(this.noContentTemplate);
	}
	
	title = 'Document Viewer - Jurada';
	
}
