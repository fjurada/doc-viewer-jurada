import { Component, OnInit, Input } from '@angular/core';
import { DocTypes } from '../doctypes';

@Component({
  selector: 'app-adddocument',
  templateUrl: './adddocument.component.html',
  styleUrls: ['./adddocument.component.css']
})
export class AdddocumentComponent implements OnInit {
	
	@Input() docName_docform:string;
	@Input() docType_docform:string;
	
	
	docTypesObj:DocTypes = new DocTypes();
	docTypes:any = this.docTypesObj.DOCTYPE;
	
	showing:boolean = false;
	docFormInputErr:boolean = false;

	constructor() { }
	
	showModal() {
		this.showing = true;
	}
	
	hideModal() {
		this.showing = false;
	}
	
	clearForm() {
		this.docName_docform = "";
		this.docType_docform = "";
	}

	ngOnInit() {
	}
	
	postDocument() {
		if (this.fieldEmpty(this.docName_docform) || this.fieldEmpty(this.docType_docform)) {
			console.log("Document posting failed");
			this.docFormInputErr = true;
		} else {	
			console.log("Posting document - fake")
			this.docFormInputErr = false;
			this.clearForm();
			this.hideModal();
		}
	}
	
	fieldEmpty(fieldValue) {
		return (fieldValue == null || fieldValue == "");
	}

}
