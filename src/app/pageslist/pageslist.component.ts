import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pageslist',
  templateUrl: './pageslist.component.html',
  styleUrls: ['./pageslist.component.css']
})
export class PageslistComponent implements OnInit {

	constructor() { }
  
	@Input() pages:any[];
	
	@Output() selectPageEvent = new EventEmitter<any>();
	
	selectedPage:any;
	
	selectPage(page) {
		console.log("Page "+ page.ID +" selected");
		this.selectedPage = page;
		this.sendSelectedPage();
	}
	
	sendSelectedPage() {
		this.selectPageEvent.emit(this.selectedPage);
	}
	
	ngOnInit() {
	}

}
