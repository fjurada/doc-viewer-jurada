import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-addpage',
  templateUrl: './addpage.component.html',
  styleUrls: ['./addpage.component.css']
})
export class AddpageComponent implements OnInit {
	
	@Input() pageText_pageform:string;
	@Input() pageUrl_pageform:string;
	
	@Input() selectedDocument:any;
	
	showing:boolean = false;
	pageFormInputErr:boolean = false;
	
	hideModal() {
		this.showing = false;
	}
	
	showModal() {
		this.showing = true;
	}
	
	clearForm() {
		this.pageText_pageform = "";
		this.pageUrl_pageform = "";
	}
	
	postPage() {
		if (this.fieldEmpty(this.pageText_pageform) || this.fieldEmpty(this.pageUrl_pageform)) {
			// TODO: Check if URL field is a valid URL
			console.log("Document posting failed");
			this.pageFormInputErr = true;
		} else {	
			console.log("Posting document - fake")
			this.pageFormInputErr = false;
			this.clearForm();
			this.hideModal();
		}
	}
	
	
	fieldEmpty(fieldValue) {
		return (fieldValue == null || fieldValue == "");
	}

  constructor() { }

  ngOnInit() {
  }

}
